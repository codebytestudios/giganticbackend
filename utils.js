exports.isArray = (value) => {
	return (typeof value === 'object') && value && (value instanceof Array)
}

exports.isBrowser = () => {
	return typeof window === 'object'
}

exports.isFunction = (value) => {
	return typeof value === 'function'
}

exports.isObject = (value) => {
	if (value === null || typeof value !== 'object') return false
	if (value instanceof Array) return false
	if (value instanceof Boolean) return false
	if (value instanceof Date) return false
	if (value instanceof Function) return false
	if (value instanceof Number) return false
	if (value instanceof RegExp) return false
	if (value instanceof String) return false
	return true
}

exports.isString = (value) => {
	return typeof value === 'string'
}

exports.isValid = (value) => {
	return value !== undefined && value !== null
}

exports.run = (func) => {
	func()
}

exports.sleep = async (millis) => {
	return new Promise((resolve) => setTimeout(() => resolve(), millis))
}
