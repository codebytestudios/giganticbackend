const config = require('./config')
const commands = require('./commands')
const crypto = require('crypto')
const db = require('./db')
const log = require('./log')
const models = require('./models')
const utils = require('./utils')
const uuidv4 = require('uuid/v4')

exports.endpoints = []

/**
 * @typedef {String}
 */
const HttpMethod = exports.HttpMethod = {
	GET:  'GET',
	POST: 'POST',
}

// **************************************************************************************************************
// INTERNAL FUNCTIONS *******************************************************************************************
// **************************************************************************************************************

/**
 * @param {String} path
 * @param {HttpMethod} method
 * @param {Function} handler
 */
function endpoint(path, method, handler) {
	exports.endpoints.push({
		method,
		path,
		async handler(req, res) {
			res.setHeader('Content-Type', 'application/json')
			try {
				const result = await handler(req, res)
				result.ok = true
				res.end(JSON.stringify(result))
			}
			catch (e) {
				res.end(JSON.stringify({ok: false, message: e.message}))
			}
		}
	})
}

// **************************************************************************************************************
// ENDPOINTS ****************************************************************************************************
// **************************************************************************************************************

endpoint('/api/sessions/refresh', HttpMethod.POST, async (req, res) => {
	await commands.refreshSession(req.body)
})

endpoint('/api/invitecodes/get', HttpMethod.GET, async (req, res) => {
	const codes = await models.InviteCode.getAll()
	return codes
})

endpoint('/api/invitecodes/create', HttpMethod.GET, async (req, res) => {
	async function createInviteCode() {
		const inviteCode = new models.InviteCode()
		inviteCode._id = uuidv4()
		inviteCode.code = crypto.randomBytes(3).toString("hex").toUpperCase()
		inviteCode.used = false
		inviteCode.user_id = null

		await inviteCode.insert()

		return inviteCode
	}

	let codes = []
	for (let i = 0; i < 100; i++) {
		let inviteCode = await createInviteCode()
		codes.push(inviteCode.code)
	}

	return codes
})

endpoint('/api/users', HttpMethod.GET, async (req, res) => {
	const users = await models.User.getAll()
	for (let user of users) {
		delete user.passwordHash
	}
	return users
})

endpoint('/api/users/register', HttpMethod.POST, async (req, res) => {
	return await commands.registerUser(req.body)
})

endpoint('/api/users/login', HttpMethod.POST, async (req, res) => {
	return await commands.loginUser(req.body)
})