let moment = require('moment')

/**
 * @param {*...} arguments
 */
exports.error = function() {
	const args = Array.prototype.slice.apply(arguments)
	console.error.apply(console, [moment().format('YYYY/MM/DD HH:mm:ss.SSS -')].concat(args))
}

/**
 * @param {*...} arguments
 */
exports.log = function() {
	const args = Array.prototype.slice.apply(arguments)
	console.log.apply(console, [moment().format('YYYY/MM/DD HH:mm:ss.SSS -')].concat(args))
}
