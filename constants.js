/**
 * @enum
 */
exports.ChatChannel = {
	crew:    "Crew",
	general: "General",
	match:   "Match",
	team:    "Team",
	whisper: "Whisper",
}

exports.UsersInGameMode = {
	"1v1": 1,
	"2v2": 2,
	"3v3": 3,
	"4v4": 4,
	"5v5": 5,
}
