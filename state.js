module.exports = exports = {
	init() {
		const models = require('./models')
		exports.matchMaker = new models.MatchMaker()
		exports.matchManager = new models.MatchManager()
	},
	tcpUsers: [],
	crews: [],
	crewInvites: [],
	matchMaker: null,
	matchManager: null,
}
