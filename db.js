const config = require('./config')
const knex = require('knex')
const log = require('./log')

let connection

// **************************************************************************************************************
// EXTERNAL FUNCTIONS *******************************************************************************************
// **************************************************************************************************************

exports.init = async () => {
	connection = exports.connection = knex(config.database)
}

/**
 * Returns a new "builder" object thing
 * @param tableName
 * @param [tran]
 */
exports.query = function (tableName, tran) {
	let thing = connection(tableName)
	if (tran) thing.transacting(tran)
	return thing
}

exports.shutdown = async () => {
	log.log('Gracefully closing database connection')
	if (!connection) return
	connection.destroy()
}

exports.transaction = () => {
	return new Promise((resolve, reject) => {
		connection.transaction((trx) => {
			resolve(trx)
		})
	})
}
