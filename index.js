let app = require('./app')
let db = require('./db')
let fs = require('fs')
let log = require('./log')
let path = require('path')
let state = require('./state')
let tcp = require('./tcp')

process.on('SIGINT', exit)

process.on('SIGTERM', exit)

process.on('unhandledRejection', (reason, p) => {
	log.error('Unhandled Rejection at: Promise', p, 'reason:', reason)
})

async function exit() {
	log.log('Shutting down')
	app.shutdown()
	await db.shutdown()
	await tcp.shutdown()
	log.log('Exiting')
	process.exit(0)
}

function makeLogDirectoryStructure() {
	try { fs.mkdirSync(path.resolve(__dirname, 'logs')) } catch (e) {}
	try { fs.mkdirSync(path.resolve(__dirname, 'logs', 'matches')) } catch (e) {}
}

(async () => {
	try {
		makeLogDirectoryStructure()
		await db.init()
		await app.init()
		state.init()
		tcp.init()
	}
	catch (e) {
		console.error('Failed to start backend')
		console.error(e)
		await exit()
	}
})()
