module.exports = class ChatMessage {
	constructor(obj) {
		this.channel = ""
		this.username = ""
		this.message = ""
		if (obj) Object.assign(this, obj)
	}
}
