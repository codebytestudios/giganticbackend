const constants = require('../constants')
const uuidv4 = require('uuid/v4')

module.exports = class MatchMakerGroup {
	constructor(obj) {
		this._id = uuidv4()
		this.created = Date.now()
		this.teamA = [] // MatchMakerGroupItem[]
		this.teamB = [] // MatchMakerGroupItem[]
		this.gameMode = null
		this.open = true // this gets set to false when the teams are filled
		this.gameAcceptTcpUsers = []
		if (obj) Object.assign(this, obj)
	}

	async accept(tcpUser) {
		if (this.open) return
		if (this.gameAcceptTcpUsers.includes(tcpUser)) return false
		this.gameAcceptTcpUsers.push(tcpUser)
		return this.allTcpUsersHaveAcceptedGame()
	}

	allTcpUsersHaveAcceptedGame() {
		let tcpUsers = this.getTcpUsers()
		for (let tcpUser of tcpUsers) {
			if (!this.gameAcceptTcpUsers.includes(tcpUser)) return false
		}
		return true
	}

	containsTcpUser(tcpUser) {
		return !!this.getItemWithTcpUser(tcpUser)
	}

	empty() {
		return this.teamA.length === 0 && this.teamB.length === 0
	}

	getItemWithTcpUser(tcpUser) {
		for (let item of this.teamA) {
			if (item.containsTcpUser(tcpUser)) return item
		}

		for (let item of this.teamB) {
			if (item.containsTcpUser(tcpUser)) return item
		}
	}

	getTcpUsers() {
		let tcpUsers = []

		for (let item of this.teamA) {
			for (let tcpUser of item.getTcpUsers()) {
				tcpUsers.push(tcpUser)
			}
		}

		for (let item of this.teamB) {
			for (let tcpUser of item.getTcpUsers()) {
				tcpUsers.push(tcpUser)
			}
		}

		return tcpUsers
	}

	getTeamThatItemCanFitInto(item) {
		if (!this.open) return
		if (item.gameMode !== this.gameMode) return

		let usersPerTeam = constants.UsersInGameMode[this.gameMode]

		if (this.teamA.reduce((total, i) => total + i.getTcpUsers().length, 0) + item.getTcpUsers().length <= usersPerTeam)
			return this.teamA

		if (this.teamB.reduce((total, i) => total + i.getTcpUsers().length, 0) + item.getTcpUsers().length <= usersPerTeam)
			return this.teamB
	}

	async removeItemWithTcpUser(tcpUser) {
		for (let team of [this.teamA, this.teamB]) {
			for (let item of team.slice()) {
				if (!item.containsTcpUser(tcpUser)) continue
				team.splice(team.indexOf(item), 1)
				await item.sendMatchMakingCanceledMessage()
				if (this.open) return
				await this.sendMatchCanceledMessage()
				this.open = true
				this.gameAcceptTcpUsers = []
				// TODO: +1 to a value on tcpUser to keep track of how many "ready matches" they've caused to cancel
				// TODO: so that we can delay when they can join matchmaking again (to prevent them from spam cancelling matches)
				// TODO: we'll want to think about how to works when that user is in a Crew, since we probably don't want to punish the whole Crew
				return
			}
		}
	}

	async sendMatchCanceledMessage() {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			promises.push(tcpUser.tcpSession.sendMessage("matchmaking.matchCanceled", {}))
		}
		await Promise.all(promises)
	}

	async sendMatchFoundMessage() {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			promises.push(tcpUser.tcpSession.sendMessage("matchmaking.matchFound", {}))
		}
		await Promise.all(promises)
	}

	teamsFull() {
		const usersPerTeam = constants.UsersInGameMode[this.gameMode]
		const teamAFull = this.teamA.reduce((total, i) => total + i.getTcpUsers().length, 0) === usersPerTeam
		const teamBFull = this.teamB.reduce((total, i) => total + i.getTcpUsers().length, 0) === usersPerTeam
		return teamAFull && teamBFull
	}

	/**
	 * Returns true if we could fit the item into one of our teams
	 * @param {MatchMakerGroupItem} item
	 * @returns {Promise<Boolean>}
	 */
	async tryToAddItem(item) {
		if (!this.open) return false
		let team = this.getTeamThatItemCanFitInto(item)
		if (!team) return false
		team.push(item)
		await item.sendMatchMakingStartedMessage()
		if (!this.teamsFull()) return true
		this.open = false
		await this.sendMatchFoundMessage()
		return true
	}
}
