const db = require('../db')
const uuidv4 = require('uuid/v4')

module.exports = class User {
	constructor(obj) {
		this._id = uuidv4()
		this.email = ''
		this.username = ''
		this.passwordHash = ''
		this.profileIconSrc = ''
		if (obj) Object.assign(this, obj)
	}

	static async existsById(id) {
		let rows = await db.query('users')
			.select('_id')
			.where('_id', id)
			.limit(1)
		return rows.length > 0
	}

	static async existsByUsernameOrEmail(username, email) {
		let rows = await db.query('users')
			.select('_id')
			.where('username', username).orWhere('email', email)
			.limit(1)
		return rows.length > 0
	}

	static async find(identifier, passwordHash) {
		let rows = await db.query('users')
			.select()
			.where(function() {
				this.where('username', identifier).orWhere('email', identifier)
			})
			.andWhere('passwordHash', passwordHash)
			.limit(1)
		return rows.map(row => new User(row)).pop()
	}

	static async findById(id) {
		let rows = await db.query('users')
			.select()
			.where('_id', id)
			.limit(1)
		return rows.map(row => new User(row)).pop()
	}

	static async getAll() {
		const rows = await db.query('users').select()
		return rows.map(row => new User(row))
	}

	async insert(tran) {
		await db.query('users', tran).insert(this)
	}

	static async searchByUsername(searchText) {
		let rows = await db.query('users')
			.select()
			.where(function() {
				this.where('username', 'like', `%${searchText}%`)
			})
			.limit(25)
		return rows.map(row => new User(row))
	}
}
