const db = require('../db')
const uuidv4 = require('uuid/v4')

module.exports = class Session {
	constructor(obj) {
		this._id = uuidv4()
		this.user_id = ''
		this.createdAt = new Date()
		this.refreshedAt = new Date()
		if (obj) Object.assign(this, obj)
	}

	async delete(tran) {
		await db.query('sessions', tran).delete().where({
			_id: this._id
		})
	}

	static async exists(id) {
		let rows = await db.query('sessions')
			.select('_id')
			.where('_id', id)
			.limit(1)
		return rows.length > 0
	}

	static async find(id) {
		let rows = await db.query('sessions')
			.select()
			.where('_id', id)
			.limit(1)
		return rows.map(row => new Session(row)).pop()
	}

	async insert(tran) {
		await db.query('sessions', tran).insert(this)
	}

	async refresh(tran) {
		await db.query('sessions', tran)
			.update({ refreshedAt: new Date() })
			.where('_id', this._id)
	}
}
