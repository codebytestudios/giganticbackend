const Match = require('./Match')
const log = require('../log')

module.exports = class MatchManager {
	constructor() {
		this.matches = []
		this.nextPort = 50000
	}

	getMatchByServerAuthId(id) {
		return this.matches.find(i => i.serverAuthId === id)
	}

	getMatchByServerTcpUser(tcpUser) {
		return this.matches.find(i => i.serverTcpUser === tcpUser)
	}

	async create(matchMakerGroup) {
		let match = new Match()
		this.matches.push(match)
		await match.begin(matchMakerGroup, this.nextPort)
		this.nextPort++
	}

	removeMatch(match) {
		this.matches.splice(this.matches.indexOf(match), 1)
	}

	async tryToRemoveItemWithTcpUser(tcpUser) {
		for (let match of this.matches) {
			if (await match.tryToRemoveItemWithTcpUser(tcpUser)) return
		}
	}
}
