const db = require('../db')
const uuidv4 = require('uuid/v4')

module.exports = class FriendRequest {
	constructor(obj) {
		this._id = uuidv4()
		this.createdAt = new Date()
		this.requesteeUser_id = ''
		this.requesterUser_id = ''
		if (obj) Object.assign(this, obj)
	}

	async delete(tran) {
		await db.query('friendRequests', tran).delete().where({
			_id: this._id
		})
	}

	static async existWithUserIds(userAId, userBId) {
		let rows = await db.query('friendRequests')
			.select('_id')
			.where({ requesterUser_id: userAId, requesteeUser_id: userBId })
			.orWhere({ requesterUser_id: userBId, requesteeUser_id: userAId })
			.limit(1)
		return rows.length > 0
	}

	static async findById(id) {
		let rows = await db.query('friendRequests')
			.select()
			.where('_id', id)
			.limit(1)
		return rows.map(row => new FriendRequest(row)).pop()
	}

	static async getAllForUserId(userId) {
		let rows = await db.query('friendRequests')
			.select()
			.where('requesteeUser_id', userId)
			.orWhere('requesterUser_id', userId)
			.limit(99)
		return rows.map(row => new FriendRequest(row))
	}

	async insert(tran) {
		await db.query('friendRequests', tran).insert(this)
	}
}
