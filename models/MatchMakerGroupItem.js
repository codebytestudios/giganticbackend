const uuidv4 = require('uuid/v4')

module.exports = class MatchMakerGroupItem {
	constructor(obj) {
		this._id = uuidv4()
		this.created = Date.now()
		this.obj = null // this is a TcpUser if they're queuing solo, or a Crew if they're queueing as a crew
		this.isCrew = false
		this.gameMode = null
		if (obj) Object.assign(this, obj)
	}

	containsTcpUser(tcpUser) {
		return this.isCrew ? this.obj.containsTcpUser(tcpUser) : this.obj === tcpUser
	}

	getTcpUsers() {
		return this.isCrew ? this.obj.tcpUsers : [this.obj]
	}

	async sendMatchMakingCanceledMessage() {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			promises.push(tcpUser.tcpSession.sendMessage("matchmaking.canceled", {}))
		}
		await Promise.all(promises)
	}

	async sendMatchMakingStartedMessage() {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			promises.push(tcpUser.tcpSession.sendMessage("matchmaking.started", {}))
		}
		await Promise.all(promises)
	}
}
