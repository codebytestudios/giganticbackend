const MatchMakerGroup = require('./MatchMakerGroup')
const MatchMakerGroupItem = require('./MatchMakerGroupItem')
const log = require('../log')
const state = require('../state')

module.exports = class MatchMaker {
	constructor() {
		this.groups = [] // MatchMakerGroup[]
	}

	findGroupWithTcpUser(tcpUser) {
		for (let group of this.groups) {
			if (group.containsTcpUser(tcpUser)) return group
		}
	}

	async accept(tcpUser) {
		let group = this.findGroupWithTcpUser(tcpUser)
		if (!group) return
		if (!await group.accept(tcpUser)) return
		this.groups.splice(this.groups.indexOf(tcpUser), 1)
		await state.matchManager.create(group)
	}

	async join(tcpUser, gameMode) {
		await this.leave(tcpUser)

		let item = new MatchMakerGroupItem({
			obj: tcpUser.crew || tcpUser,
			isCrew: !!tcpUser.crew,
			gameMode
		})

		for (let group of this.groups) {
			if (await group.tryToAddItem(item)) return
		}

		const group = new MatchMakerGroup({
			gameMode
		})
		this.groups.push(group)
		await group.tryToAddItem(item)
	}

	async leave(tcpUser) {
		let group = this.findGroupWithTcpUser(tcpUser)
		if (!group) return
		await group.removeItemWithTcpUser(tcpUser)
		if (!group.empty()) return
		this.groups.splice(this.groups.indexOf(group), 1)
	}
}
