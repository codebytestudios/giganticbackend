const db = require('../db')
const uuidv4 = require('uuid/v4')

module.exports = class FriendLink {
	constructor(obj) {
		this._id = uuidv4()
		this.createdAt = new Date()
		this.requesteeUser_id = ''
		this.requesterUser_id = ''
		if (obj) Object.assign(this, obj)
	}

	static async existWithUserIds(userAId, userBId) {
		let rows = await db.query('friendLinks')
			.select('_id')
			.where({ requesterUser_id: userAId, requesteeUser_id: userBId })
			.orWhere({ requesterUser_id: userBId, requesteeUser_id: userAId })
			.limit(1)
		return rows.length > 0
	}

	static async findById(id) {
		let rows = await db.query('friendLinks')
			.select()
			.where('_id', id)
			.limit(1)
		return rows.map(row => new FriendLink(row)).pop()
	}

	static async getAllForUserId(userId) {
		let rows = await db.query('friendLinks')
			.select()
			.where('requesteeUser_id', userId)
			.orWhere('requesterUser_id', userId)
			.limit(99)
		return rows.map(row => new FriendLink(row))
	}

	async insert(tran) {
		await db.query('friendLinks', tran).insert(this)
	}
}
