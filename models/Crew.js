const uuidv4 = require('uuid/v4')
const constants = require('../constants')

module.exports = class Crew {
	constructor(obj) {
		this._id = uuidv4()
		this.tcpUsers = []
		this.leaderTcpUser = null
		if (obj) Object.assign(this, obj)
	}

	containsTcpUser(tcpUser) {
		return this.tcpUsers.includes(tcpUser)
	}

	async addTcpUser(tcpUserToAdd) {
		if (this.tcpUsers.includes(tcpUserToAdd)) return
		this.tcpUsers.push(tcpUserToAdd)
		let promises = []
		let userIds = this.tcpUsers.map(i => i.user._id)

		console.log(this)

		for (let tcpUser of this.tcpUsers) {
			var message = `'${tcpUserToAdd.user.username}' joined your crew`
			if(tcpUser === tcpUserToAdd) {
				message = `You have joined ${this.leaderTcpUser.user.username}'s crew`
			}

			promises.push(tcpUser.tcpSession.sendMessage('crew.updated', {
				userIds,
				leaderUserId: this.leaderTcpUser.user._id
			}))
			promises.push(tcpUser.tcpSession.sendMessage('chat.receiveMessage', {
				channel: constants.ChatChannel.crew,
				username: "INFO",
				message: message,
			}))
		}
		await Promise.all(promises)
	}

	async removeTcpUser(user) {
		// TODO: This needs to reassign the leaderTcpUser to one of the other users
		let index = this.tcpUsers.indexOf(user)
		if (index === -1) return
		this.tcpUsers.splice(index, 1)
	}
}
