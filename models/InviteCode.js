const db = require('../db')
const uuidv4 = require('uuid/v4')

module.exports = class InviteCode {
	constructor(obj) {
		this._id = uuidv4()
		this.code = ''
		this.used = false
		this.user_id = ''
		if (obj) Object.assign(this, obj)
	}

	static async findByCode(code) {
		let rows = await db.query('inviteCodes')
			.select()
			.where('code', code)
			.andWhere('used', false)
			.limit(1)
		return rows.map(row => new InviteCode(row)).pop()
	}

	async insert(tran) {
		await db.query('inviteCodes', tran).insert(this)
	}

	async markAsUsed(userId, tran) {
		await db.query('inviteCodes', tran)
			.update({
				used: true,
				user_id: userId
			})
			.where('_id', this._id)
	}

	static async getAll() {
		const rows = await db.query('inviteCodes').select()
		return rows.map(row => new InviteCode(row))
	}
}
