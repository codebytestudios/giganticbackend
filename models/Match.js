const config = require('../config')
const constants = require('../constants')
const ChatMessage = require('./ChatMessage')
const EventEmitter = require('events')
const uuidv4 = require('uuid/v4')
const { spawn } = require('child_process')
const log = require('../log')
const path = require('path')
const state = require('../state')

module.exports = class Match extends EventEmitter {
	constructor(obj) {
		super()
		this._id = uuidv4()
		this.serverAuthId = uuidv4()
		this.serverPort = null
		this.serverTcpUser = null
		this.gameMode = null
		this.teamA = [] // MatchMakerGroupItem[]
		this.teamB = [] // MatchMakerGroupItem[]
		this.process = null
		if (obj) Object.assign(this, obj)
	}

	async begin(matchMakerGroup, serverPort) {
		this.gameMode = matchMakerGroup.gameMode
		this.teamA = matchMakerGroup.teamA
		this.teamB = matchMakerGroup.teamB
		this.serverPort = serverPort

		this.process = spawn(`${config.serverBinaryPath}`, [
			'-batchmode', '-nographics', '-logfile', path.resolve(__dirname, '..', 'logs', 'matches', this._id + '.log'), '-server', this.serverAuthId
		])

		this.process.on('close', (code) => {
			log.log(`Game server closed: ${this._id}`)
			state.matchManager.removeMatch(this)
		})
	}

	empty() {
		return this.teamA.length === 0 && this.teamB.length === 0
	}

	getItemWithTcpUser(tcpUser) {
		for (let item of this.teamA) {
			if (item.containsTcpUser(tcpUser)) return item
		}

		for (let item of this.teamB) {
			if (item.containsTcpUser(tcpUser)) return item
		}
	}

	getTcpUsers() {
		let tcpUsers = []

		for (let item of this.teamA) {
			for (let tcpUser of item.getTcpUsers()) {
				tcpUsers.push(tcpUser)
			}
		}

		for (let item of this.teamB) {
			for (let tcpUser of item.getTcpUsers()) {
				tcpUsers.push(tcpUser)
			}
		}

		return tcpUsers
	}

	async sendJoinServerMessage() {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			// TODO: ip will eventually point to the temporary virtual server we spin up for each game
			promises.push(tcpUser.tcpSession.sendMessage('server.join', { ip: "104.231.66.214", port: this.serverPort }))
		}
		await Promise.all(promises)
	}

	/**
	 * @param {ChatMessage} message
	 * @returns {Promise<void>}
	 */
	async sendMessage(message) {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			promises.push(tcpUser.tcpSession.sendMessage('chat.receiveMessage', message))
		}
		await Promise.all(promises)
	}

	/**
	 * @param {ChatMessage[]} messages
	 * @returns {Promise<void>}
	 */
	async sendMessages(messages) {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			promises.push(tcpUser.tcpSession.sendMessage('chat.receiveMessages', { messages }))
		}
		await Promise.all(promises)
	}

	async sendServerStartingMessage() {
		let promises = []
		for (let tcpUser of this.getTcpUsers()) {
			promises.push(tcpUser.tcpSession.sendMessage('server.starting', {}))
		}
		await Promise.all(promises)
	}

	/**
	 * Returns true if we removed the item with the passed in tcpUser
	 * @param {TcpUser} tcpUser
	 * @returns {Promise<Boolean>}
	 */
	async tryToRemoveItemWithTcpUser(tcpUser) {
		for (let team of [this.teamA, this.teamB]) {
			for (let item of team.slice()) {
				if (!item.containsTcpUser(tcpUser)) continue
				team.splice(team.indexOf(item), 1)

				let messages = []
				let itemTcpUsers = item.getTcpUsers()

				// tell all the users in that item to leave the game
				let promises = []
				for (let itemTcpUser of itemTcpUsers) {
					messages.push(new ChatMessage({
						channel: constants.ChatChannel.match,
						username: "SERVER",
						message: `'${itemTcpUser.user.username}' disconnected`,
					}))
					if (itemTcpUser === tcpUser) continue
					promises.push(itemTcpUser.tcpSession.sendMessage('game.leave', { causedByUserId: tcpUser.user._id }))
				}
				await Promise.all(promises)

				await this.sendMessages(messages)

				// TODO: +1 to a value on tcpUser to keep track of how many "ready matches" they've caused to cancel
				// TODO: so that we can delay when they can join matchmaking again (to prevent them from spam cancelling matches)
				// TODO: we'll want to think about how to works when that user is in a Crew, since we probably don't want to punish the whole Crew

				if (this.empty()) this.process.kill()

				return true
			}
		}

		return false
	}
}
