// Note: model files shouldn't require this file, they should just include each other directly
module.exports = {
	Crew: require('./Crew'),
	CrewInvite: require('./CrewInvite'),
	FriendLink: require('./FriendLink'),
	FriendRequest: require('./FriendRequest'),
	InviteCode: require('./InviteCode'),
	Match: require('./Match'),
	MatchMaker: require('./MatchMaker'),
	MatchMakerGroup: require('./MatchMakerGroup'),
	MatchMakerGroupItem: require('./MatchMakerGroupItem'),
	MatchManager: require('./MatchManager'),
	Session: require('./Session'),
	User: require('./User'),
}