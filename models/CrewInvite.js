const uuidv4 = require('uuid/v4')

module.exports = class CrewInvite {
	constructor(obj) {
		this._id = uuidv4()
		this.crew_id = ''
		this.invitedUser_id = ''
		this.leaderUser_id = ''
		if (obj) Object.assign(this, obj)
	}
}
