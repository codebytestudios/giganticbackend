const fs = require('fs')
const path = require('path')

// *********************************************************************************************************************
// EXPORTED PROPERTIES *************************************************************************************************
// *********************************************************************************************************************

exports.timezone = 'America/Los_Angeles'
exports.database = { // this is passed directly to knex
	client:     'postgres',
	connection: {
		host:     '127.0.0.1',
		user:     'postgres',
		password: '',
		database: 'gigantic'
	}
}
exports.serverBinaryPath = ''

// *********************************************************************************************************************
// CONFIG LOADING ******************************************************************************************************
// *********************************************************************************************************************

const configPath = path.resolve(path.join(__dirname, 'config.json'))

try {
	fs.accessSync(configPath, fs.F_OK)
}
catch (e) {
	console.log(`A config file with default values has been generated at ${configPath}`)
	fs.writeFileSync(configPath, JSON.stringify(exports, null, '\t'))
	process.exit(0)
}

try {
	Object.assign(exports, JSON.parse(fs.readFileSync(configPath)))
	fs.writeFileSync(configPath, JSON.stringify(exports, null, '\t'))
}
catch (e) {
	console.error('Failed to load config.json')
	console.error(e)
	process.exit(0)
}
