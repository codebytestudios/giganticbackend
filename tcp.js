const commands = require('./commands')
const eventbus = require('./eventbus')
const EventEmitter = require('events')
const log = require('./log')
const models = require('./models')
const net = require('net')
const state = require('./state')
const utils = require('./utils')
const uuidv4 = require('uuid/v4')
const constants = require('./constants')

let server
let running = false

async function tcpUserIsLoggedInAndSessionIsGood(tcpUser) {
	if (!tcpUser.user) throw new Error('User is not logged in')
	return await commands.validateSession(tcpUser.sessionId)
}

class TcpMessageProcessor extends EventEmitter {
	constructor() {
		super()
		this.receivingPayloadLength = true
		this.payloadLengthBuffer = Buffer.alloc(2)
		this.payloadLengthByteIndex = 0
		this.payloadBuffer = Buffer.alloc(65536)
		this.payloadBufferLength = 0
		this.payloadByteIndex = 0
	}

	processChunk(chunk) {
		for (let byte of chunk) {
			if (this.receivingPayloadLength) {
				this.payloadLengthBuffer[this.payloadLengthByteIndex] = byte
				this.payloadLengthByteIndex++
				if (this.payloadLengthByteIndex === 2) {
					this.receivingPayloadLength = false
					this.payloadBufferLength = this.payloadLengthBuffer[0] + (this.payloadLengthBuffer[1] << 8)
					this.payloadByteIndex = 0
				}
				continue
			}

			this.payloadBuffer[this.payloadByteIndex] = byte
			this.payloadByteIndex++

			if (this.payloadByteIndex < this.payloadBufferLength) continue

			this.payloadLengthByteIndex = 0
			this.receivingPayloadLength = true
			this.emit('message', this.payloadBuffer.slice(0, this.payloadBufferLength).toString('utf8'))
		}
	}
}

class TcpSession {
	constructor(socket) {
		this.onConnected = undefined
		this.onDisconnected = undefined
		this.onProcessRequest = undefined

		this.responseHandlers = {}

		this.tcpMessageProcessor = new TcpMessageProcessor()
		this.tcpMessageProcessor.on('message', async (message) => {
			try {
				const payload = JSON.parse(message)
				try {
					await this.handleMessage(payload)
				}
				catch (e) {
					log.error(`Failed to handle message: ${e.message}`)
				}
			}
			catch (e) {
				log.error(`Failed to parse message from ${this.socket.remoteAddress}`)
			}
		})

		this.socket = socket
		socket.on('data', (chunk) => {
			this.tcpMessageProcessor.processChunk(chunk)
		})
	}

	/**
	 * The request payload will always look like this:
	 * { action: String, requestId: String, data: Object }
	 *
	 * And the response payload should always look like:
	 * { action: String, responseId: String, data: Object }
	 *
	 * @param {Object} payload
	 * @returns {Promise<void>}
	 */
	async handleMessage(payload) {
		if (!utils.isString(payload.action))
			throw new Error('Payload missing action')

		if (payload.action === '__response') {
			const responseHandler = this.responseHandlers[payload.responseId]
			if (!responseHandler) return log.error(`Failed to find response handler`)
			responseHandler(payload.data)
			return
		}

		let data

		try {
			data = await this.onProcessRequest(payload.action, payload.data) || {}
			if (utils.isObject(data)) {
				data.ok = true
			}
			else {
				log.error(`'onProcessRequest' for action '${payload.action}' needs to return an object`)
				data = {
					ok: false,
					message: 'Internal server error.'
				}
			}
		}
		catch (e) {
			data = {
				ok: false,
				message: e.message
			}
		}

		await this.sendMessage('__response', data, payload.requestId)
	}

	/**
	 * @param {String} action
	 * @param {Object} data
	 * @param {String} [responseId] - only pass this in if we're responding to a request
	 * @returns {Promise<void>}
	 */
	async sendMessage(action, data, responseId) {
		const payload = { action, data }

		if (utils.isString(responseId))
			payload.responseId = responseId
		else
			payload.requestId = uuidv4()

		const payloadString = JSON.stringify(payload)

		if (payloadString.length > 65536)
			throw new Error('Payload length must be less than or equal to 65536')

		let buffer = Buffer.alloc(2 + payloadString.length)
		buffer[0] = payloadString.length & 0x000000FF
		buffer[1] = (payloadString.length & 0x0000FF00) >> 8
		buffer.write(payloadString, 2, payloadString.length, 'utf8')

		return new Promise((resolve, reject) => {
			// we're sending a request, so add a handler for it later
			if (!utils.isString(responseId)) {
				this.responseHandlers[payload.requestId] = resolve
			}

			try {
				this.socket.write(buffer, () => {
					if (utils.isString(responseId)) resolve()
				})
			}
			catch (e) {
				log.log('Failed to write to socket')
				log.error(e)
			}
		})
	}
}

class TcpUser {
	constructor(socket) {
		this.authenticatedAsServer = false
		this.crew = null
		this.user = null
		this.sessionId = null
		this.lastPing = Date.now()

		this.tcpSession = new TcpSession(socket)

		this.tcpSession.onConnected = async () => {
		}

		this.tcpSession.onProcessRequest = async (action, data) => {
			const processor = this.requestProcessors[action]
			if (!processor) throw new Error('Invalid action')
			return await processor(data)
		}

		this.tcpSession.onDisconnected = async () => {
			if (this.crew) await this.crew.removeTcpUser(this)
			// TODO: Delete the crew if it's empty (remove it from matchmaking while we're at it)

			await state.matchMaker.leave(this)
			await state.matchManager.tryToRemoveItemWithTcpUser(this)

			// TODO: Remove all CrewInvites for this user
		}

		this.requestProcessors = {
			"chat.sendMessage": async (data) => {
				let { channel, message } = data
				for (let tcpUser of state.tcpUsers) {
					//if (this === tcpUser) continue
					tcpUser.tcpSession.sendMessage('chat.receiveMessage', {
						channel,
						message,
						username: this.user.username,
					})
				}
			},
			"crews.acceptInvite": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)

				let { crewInviteId } = data
				if (!crewInviteId) throw new Error(`'crewInviteId' parameter is required`)

				let crewInvite = state.crewInvites.find(i => i._id === crewInviteId)
				if (!crewInvite) throw new Error('Crew invite with that id does not exist')

				state.crewInvites.splice(state.crewInvites.indexOf(crewInvite), 1)

				let tcpUserToJoin = state.tcpUsers.find(i => i.user && i.user._id === crewInvite.leaderUser_id)
				if (!tcpUserToJoin) throw new Error('The crew leader is no longer online')

				// See if user is in a crew. If they are remove them, send a message to old crew members, update the old crews clients.
				let oldCrew = state.crews.find(i => i.tcpUsers.find(i => i === this))
				if (oldCrew) {
					let promises = []
					let userIds = oldCrew.tcpUsers.map(i => i.user._id)
					await oldCrew.removeTcpUser(this)
					for (let tcpUser of oldCrew.tcpUsers) {
						promises.push(tcpUser.tcpSession.sendMessage('chat.receiveMessage', {
							channel: constants.ChatChannel.crew,
							username: "INFO",
							message: `'${tcpUserToJoin.user.username}' has left your crew`,
						}))
						promises.push(tcpUser.tcpSession.sendMessage('crew.updated', {
							userIds,
							leaderUserId: oldCrew.leaderTcpUser.user._id
						}))
					}
					await Promise.all(promises)
				}

				let crew = state.crews.find(i => i._id === crewInvite.crew_id)

				if (!crew) {
					state.crews.push(crew = new models.Crew({
						leaderTcpUser: tcpUserToJoin
					}))
					crew.addTcpUser(tcpUserToJoin)
				}

				crew.addTcpUser(this)
				this.crew = crew

			},
			"crews.declineInvite": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)

				let { crewInviteId } = data
				if (!crewInviteId) throw new Error(`'crewInviteId' parameter is required`)

				let crewInvite = state.crewInvites.find(i => i._id === crewInviteId)
				if (!crewInvite) throw new Error('Crew invite with that id does not exist')

				console.log(state.crewInvites)
				state.crewInvites.splice(state.crewInvites.indexOf(crewInvite), 1)
				console.log(state.crewInvites)
			},
			"crew.getUserInfo": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)
				const User = models.User

				let { userId } = data
				if (!userId) throw new Error(`'userId' parameter is required`)

				let user = await User.findById(userId)
				if (!user) throw new Error('User with that id does not exist')

				delete user.passwordHash
				delete user.email

				return { user }
			},
			"crews.inviteUser": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)

				console.log(state.crews)

				let { userId } = data
				if (!userId) throw new Error(`'userId' parameter is required`)

				console.log(userId)

				let tcpUser = state.tcpUsers.find(i => i.user && i.user._id === userId)
				if (!tcpUser) throw new Error('That user is no longer online')

				console.log(tcpUser)

				let crew = this.crew ? this.crew.find(i => i.tcpUsers.find(i => i === tcpUser)) : null
				if (crew) throw new Error('That user is already in your crew dumbass')

				console.log(crew)

				// check if leader already has a crew
				let currentCrew = state.crews.find(i => i.tcpUsers.find(i => i === this))

				let crewInvite = new models.CrewInvite({
					invitedUser_id: tcpUser.user._id,
					leaderUser_id: this.user._id,
					crew_id: currentCrew != null ? currentCrew._id : null
				})

				state.crewInvites.push(crewInvite)

				tcpUser.tcpSession.sendMessage('crew.receiveInvite', {
					crewInvite,
					username: this.user.username,
				})
			},
			"crews.leave": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)
				let oldCrew = state.crews.find(i => i.tcpUsers.find(i => i === this))
				if (oldCrew) await oldCrew.removeTcpUser(this)
				// TODO: Delete the crew if it's empty (remove it from matchmaking while we're at it)
			},
			"friends.search": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)
				let result = await commands.searchUsers(data)
				return result
			},
			"friends.getFriendsList": async (data) => {
				const User = models.User
				await tcpUserIsLoggedInAndSessionIsGood(this)

				let friendsList = await models.FriendLink.getAllForUserId(this.user._id)

				for (let friend of friendsList) {
					let friendId = friend.requesterUser_id === this.user._id ? friend.requesteeUser_id : friend.requesterUser_id
					let user = await User.findById(friendId)
					friend.friendUsername = user.username
					friend.friendId = user._id
					friend.profileIconSrc = user.profileIconSrc
				}

				return { friendsList }
			},
			"friends.getFriendRequests": async (data) => {
				const User = models.User
				await tcpUserIsLoggedInAndSessionIsGood(this)
				let friendRequests = await models.FriendRequest.getAllForUserId(this.user._id)

				for (let request of friendRequests) {
					let user = await User.findById(request.requesterUser_id)
					request.requesterUsername = user.username
					request.requesterProfileIconSrc = user.profileIconSrc

					user = await User.findById(request.requesteeUser_id)
					request.requesteeUsername = user.username
					request.requesteeProfileIconSrc = user.profileIconSrc
				}

				// if(!friendsList) throw new Error(`No Friends Found`)
				return { friendRequests }
			},
			"friends.sendFriendRequest": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)

				let { requesteeUserId } = data;
				let tcpUserToSend;
				data.requesterUserId = this.user._id;

				let result = await commands.sendFriendRequest(data)

				if (result) {
					tcpUserToSend = state.tcpUsers.find(i => i.user && i.user._id === requesteeUserId)
					tcpUserToSend.tcpSession.sendMessage('friend.update', {})
				}

				return result

			},
			"friends.acceptFriendRequest": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)

				let { friendRequestId } = data;

				let result = await commands.acceptFriendRequest(data)

				return result

			},
			"friends.declineFriendRequest": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)

				let { friendRequestId } = data;

				let result = await commands.declineFriendRequest(data)

				return result

			},
			"matchmaking.accept": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)
				await state.matchMaker.accept(this)
			},
			"matchmaking.join": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)

				let { gameMode } = data
				if (!gameMode) throw new Error(`'gameMode' parameter is required`)

				await state.matchMaker.join(this, gameMode)
			},
			"matchmaking.leave": async (data) => {
				await tcpUserIsLoggedInAndSessionIsGood(this)
				await state.matchMaker.leave(this)
			},
			"ping": async (data) => {
				this.lastPing = Date.now()
				return {}
			},
			"server.authenticate": async (data) => {
				let { serverAuthId } = data
				if (!serverAuthId) throw new Error(`'serverAuthId' parameter is required`)

				let match = state.matchManager.getMatchByServerAuthId(serverAuthId)
				if (!match) throw new Error(`No matches found with the passed serverAuthId`)

				this.authenticatedAsServer = true
				match.serverTcpUser = this

				await match.sendServerStartingMessage()

				return { serverPort: match.serverPort }
			},
			"server.started": async (data) => {
				log.log('Game server initialized! Starting game!')
				let match = state.matchManager.getMatchByServerTcpUser(this)
				if (!match) throw new Error(`No match found`)
				await match.sendJoinServerMessage()
			},
			"sessions.refresh": async (data) => {
				const User = models.User

				let { session } = await commands.refreshSession(data)

				let user = await User.findById(session.user_id)
				if (!user) {
					await session.delete()
					throw new Error('User for session no longer exists.')
				}

				this.user = user
				this.sessionId = session._id
			},
			"users.login": async (data) => {
				let result = await commands.loginUser(data)
				log.log(`"${result.user.username}" logged in via game client`)
				this.user = result.user
				this.sessionId = result.sessionId
				return result
			},
			"users.register": async (data) => {
				return await commands.registerUser(data)
			},
		}
	}
}

// **************************************************************************************************************
// INTERNAL FUNCTIONS *******************************************************************************************
// **************************************************************************************************************

// **************************************************************************************************************
// EXTERNAL FUNCTIONS *******************************************************************************************
// **************************************************************************************************************

exports.init = () => {
	running = true

	server = net.createServer(async (socket) => {
		log.log(`Client connected ${socket.remoteAddress}`)

		const user = new TcpUser(socket)
		state.tcpUsers.push(user)
		if (utils.isFunction(user.tcpSession.onConnected))
			await user.tcpSession.onConnected()

		socket.on('end', async () => {
			state.tcpUsers.splice(state.tcpUsers.indexOf(user), 1)
			if (utils.isFunction(user.tcpSession.onDisconnected))
				await user.tcpSession.onDisconnected()
			log.log(`Client disconnected ${socket.remoteAddress}`)
		})
	})

	server.on('close', () => {
		log.log(`TCP server closed`)
	})

	server.listen(8081, () => {
		log.log(`TCP server bound to port ${server.address().port}`)
	})

	eventbus.on('friendRequests.create', async (friendRequest) => {
		let tcpUser = state.tcpUsers.find(tcpUser => tcpUser.user && tcpUser.user._id === friendRequest.requesteeUser_id)
		if (!tcpUser) return

		let requesterUser = await models.User.findById(friendRequest.requesterUser_id)
		if (!requesterUser) return

		await tcpUser.tcpSession.sendMessage("friendRequestsCreate", {
			requesterUsername: requesterUser.username,
			friendRequest,
		})
	})

	// client timeout loop
	utils.run(async () => {
		while (running) {
			const now = Date.now()
			for (let user of state.tcpUsers.slice()) {
				if (now - user.lastPing < 1000 * 60) continue
				let socket = user.tcpSession.socket
				log.log(`Client timed out ${socket.remoteAddress}`)
				socket.end()
			}
			await utils.sleep(5000)
		}
	})
}

exports.shutdown = () => {
	running = false
	log.log(`Closing TCP server`)
	return new Promise(async (resolve, reject) => {
		for (let user of state.tcpUsers)
			user.tcpSession.socket.end()
		server.close(() => resolve())
	})
}
