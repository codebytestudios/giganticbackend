const crypto = require('crypto')
const db = require('./db')
const eventbus = require('./eventbus')
const models = require('./models')

exports.acceptFriendRequest = async (payload) => {
	const FriendRequest = models.FriendRequest
	const FriendLink = models.FriendLink
	const User = models.User

	let { friendRequestId } = payload

	if (!friendRequestId) throw new Error(`'friendRequestId' parameter is required`)

	let friendRequest = await FriendRequest.findById(friendRequestId)

	if (!friendRequest) throw new Error('A friend request with that id does not exist.')

	if (!await User.existsById(friendRequest.requesteeUser_id))
		throw new Error('The requestee user no longer exists.')

	if (!await User.existsById(friendRequest.requesterUser_id))
		throw new Error('The requester user no longer exists.')

	let friendLink = new FriendLink({
		requestedAt: friendRequest.createdAt,
		requesteeUser_id: friendRequest.requesteeUser_id,
		requesterUser_id: friendRequest.requesterUser_id,
	})

	const tran = await db.transaction()
	try {
		await friendLink.insert(tran)
		await friendRequest.delete(tran)
		await tran.commit()
	}
	catch (e) {
		await tran.rollback()
		throw e
	}

	return { friendLink }
}

exports.declineFriendRequest = async (payload) => {
	const FriendRequest = models.FriendRequest

	let { friendRequestId } = payload

	if (!friendRequestId) throw new Error(`'friendRequestId' parameter is required`)

	let friendRequest = await FriendRequest.findById(friendRequestId)

	if (!friendRequest) throw new Error('A friend request with that id does not exist.')

	await friendRequest.delete()

	return { friendRequest }
}

exports.sendFriendRequest = async (payload) => {
	const FriendRequest = models.FriendRequest
	const FriendLink = models.FriendLink
	const User = models.User

	let { requesteeUserId, requesterUserId } = payload

	if (!requesteeUserId) throw new Error(`'requesteeUserId' parameter is required`)
	if (!requesterUserId) throw new Error(`'requesterUserId' parameter is required`)

	if (requesteeUserId === requesterUserId)
		throw new Error("A user can't be friends with themselves.")

	if (!await User.existsById(requesteeUserId))
		throw new Error('The requestee user does not exist.')

	if (!await User.existsById(requesterUserId))
		throw new Error('The requester user does not exist.')

	if (await FriendLink.existWithUserIds(requesterUserId, requesteeUserId))
		throw new Error('These users are already friends.')

	if (await FriendRequest.existWithUserIds(requesterUserId, requesteeUserId))
		throw new Error('A friend request between these users has already been created.')

	let friendRequest = new FriendRequest({
		requesteeUser_id: requesteeUserId,
		requesterUser_id: requesterUserId,
	})

	await friendRequest.insert()

	eventbus.emit('friendRequests.create', friendRequest)

	return { friendRequest }
}

exports.searchUsers = async (payload) => {
	const User = models.User

	let { searchText } = payload

	if (!searchText) throw new Error(`'searchText' parameter is required`)
	let users = await User.searchByUsername(searchText)

	return {
		results: users.map(user => {
			return {
				_id: user._id,
				username: user.username,
				profileIconSrc: user.profileIconSrc,
			}
		})
	}
}

exports.loginUser = async (payload) => {
	const User = models.User
	const FriendLink = models.FriendLink

	let { identifier, password } = payload

	if (!identifier) throw new Error(`'identifier' parameter is required`)
	if (!password) throw new Error(`'password' parameter is required`)

	const passwordHash = crypto.createHash('sha256').update(password).digest('base64')

	const user = await User.find(identifier, passwordHash)

	if (!user) throw new Error('Failed to find user.')

	delete user.passwordHash

	const session = new models.Session()
	session.user_id = user._id

	await session.insert()

	const friends = FriendLink.getAllForUserId(user._id);

	return { sessionId: session._id, user, friends }
}

exports.refreshSession = async (payload) => {
	const Session = models.Session

	let { sessionId } = payload

	if (!sessionId) throw new Error(`'sessionId' parameter is required`)

	const session = await Session.find(sessionId)
	if (!session) throw new Error('Invalid session.')

	await session.refresh()

	return { session }
}

exports.registerUser = async (payload) => {
	const ProfileIcons = [
		'profile_icon_badges_eye',
		'profile_icon_badges_flower',
		'profile_icon_badges_king',
		'profile_icon_badges_wingblade',
		'profile_icon_FC_death',
		'profile_icon_FC_jester',
		'profile_icon_FC_judgment',
		'profile_icon_FC_justice',
		'profile_icon_founders_aurion',
		'profile_icon_founders_devaedra',
		'profile_icon_guardian_griffin',
		'profile_icon_guardian_griffin2',
		'profile_icon_guardian_naga',
		'profile_icon_guardian_naga2',
		'profile_icon_hero_alchemist',
		'profile_icon_hero_bombard',
		'profile_icon_hero_machine',
		'profile_icon_hero_warden',
		'profile_icon_herotheme_alchemist',
		'profile_icon_herotheme_angel',
		'profile_icon_herotheme_bombard',
		'profile_icon_herotheme_frosty',
		'profile_icon_herotheme_judo',
		'profile_icon_herotheme_machine',
		'profile_icon_herotheme_quarrel',
		'profile_icon_herotheme_swift',
		'profile_icon_house_aurion',
		'profile_icon_house_devaedra',
		'profile_icon_misc_circles',
		'profile_icon_misc_crown',
		'profile_icon_misc_featherarrow',
		'profile_icon_misc_flame',
		'profile_icon_misc_helmet',
		'profile_icon_misc_star',
	]
	const InviteCode = models.InviteCode
	const User = models.User
	const RandomIconIndex = Math.floor(Math.random() * 33) + 0

	let { email, username, password, inviteCode } = payload

	if (!email) throw new Error(`'email' parameter is required`)
	if (!username) throw new Error(`'username' parameter is required`)
	if (!password) throw new Error(`'password' parameter is required`)
	//if (!inviteCode) throw new Error(`'inviteCode' parameter is required`)

	/* const inviteCodeObj = await InviteCode.findByCode(inviteCode)
	if (!inviteCodeObj || inviteCodeObj.used)
		throw new Error('Invalid invite code.')*/

	if (await User.existsByUsernameOrEmail(username, email))
		throw new Error('User with that username or email already exist.')

	const user = new models.User()
	user.email = email
	user.username = username
	user.passwordHash = crypto.createHash('sha256').update(password).digest('base64')
	user.profileIconSrc = ProfileIcons[RandomIconIndex]

	await user.insert()
	//await inviteCodeObj.markAsUsed(user._id)

	delete user.passwordHash

	return { user }
}

exports.validateSession = async (sessionId) => {
	const Session = models.Session
	if (!sessionId) throw new Error('Invalid session.')
	const session = await Session.find(sessionId)
	if (!session) throw new Error('Invalid session.')
	return session
}
