const bodyParser = require('body-parser')
const express = require('express')
const log = require('./log')
const endpoints = require('./endpoints')

// **************************************************************************************************************
// EXPORTED FUNCTIONS *******************************************************************************************
// **************************************************************************************************************

exports.init = async () => {
	log.log('Initializing web server')

	const app = exports.app = express()

	app.use(bodyParser.json({limit: 1024 * 1024 * 3}))

	// this guy should always be last
	app.use((err, req, res, next) => {
		res.status(500).end(JSON.stringify({ok: false, message: err.message}, null, '\t'))
	})

	// setup all the endpoints
	endpoints.endpoints.forEach(endpoint => {
		switch (endpoint.method) {
			case endpoints.HttpMethod.GET:
				app.get(endpoint.path, endpoint.handler)
				break
			case endpoints.HttpMethod.POST:
				app.post(endpoint.path, endpoint.handler)
				break
		}
	})

	exports.server = app.listen(8080, () => {
		log.log('Web server listening on localhost:8080')
	})
}

exports.shutdown = () => {
	if (!exports.app) return
	log.log('Gracefully shutting down web server')
	exports.server.close()
}
